package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;

public class StringTextfeld extends UiElement<String> {

	public StringTextfeld(String value, Field zugehörigesFeld) {
		super(value, zugehörigesFeld);
	}
}
