package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

public abstract class UiElement<T> {

	
	private T instanceValue;
	private final Field zugehörigesFeld;

	public UiElement(T instanceValue, Field zugehörigesFeld) {
		this.setInstanceValue(instanceValue);
		this.zugehörigesFeld = zugehörigesFeld;

	}

	/**
	 * @return Das UI Element darf nicht modifiziert werden können wenn es final
	 *         deklariert worden ist.
	 */
	public boolean isFinal() {
		return Modifier.isFinal(getZugehörigesFeld().getModifiers());
	}

	public List<Annotation> getAnnotations() {
		return Arrays.asList(getZugehörigesFeld().getAnnotations());
	}

	/**
	 * @return das Feld aus dem das UI-Element erzeugt worden ist.
	 */
	public Field getZugehörigesFeld() {
		return zugehörigesFeld;
	}

	/**
	 * @return null oder eine Instanz des Klasse, aus welchem das jeweilige
	 *         UIElement erstellt worden ist.
	 */
	public T getInstanceValue() {
		return instanceValue;
	}

	public void setInstanceValue(T instance) {
		this.instanceValue = instance;
	}
	
	public String getLabel(){
		return this.getZugehörigesFeld().getName();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName()+ " [instanceValue=" + instanceValue + ", zugehörigesFeld=" + zugehörigesFeld.getName() + "]\n";
	}

}
