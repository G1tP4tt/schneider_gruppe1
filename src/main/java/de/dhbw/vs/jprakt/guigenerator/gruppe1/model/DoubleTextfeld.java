package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;

public class DoubleTextfeld extends UiElement<Double> {

	public DoubleTextfeld(Double value, Field zugehörigesFeld) {
		super(value,  zugehörigesFeld);
	}

}
