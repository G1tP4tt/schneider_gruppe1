package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.dhbw.vs.jprakt.guigenerator.annotations.Id;

/*Key wird annotiert (@ID)
 *Wenn keine Annotation vorhanden einfach durchnummerien
 *Key kann beliebiges Feld im AbhObjekt sein
*/
/**
 * @author User
 *
 * @param <T>
 */
public class Objektliste<T> extends UiElement<List<T>> {

	private Map<String, T> objMap = new TreeMap<>();
	private int keyCount = 0;

	public Objektliste(List<T> instanceValue, Field zugehörigesFeld)
			throws IllegalArgumentException, IllegalAccessException {
		super(instanceValue, zugehörigesFeld);
		this.updateMap();
	}

	/**
	 * Synchronizes this.objMap with List<T> instanceValue. updateMap() sets
	 * Keys in objMap according to the idField. If no idField is present an
	 * integer key is chosen instead.
	 * 
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public void updateMap() throws IllegalArgumentException, IllegalAccessException {
		Map<String, T> result = new HashMap<>();
		Field idField = this.getIdField();

		if (this.getInstanceValue() != null) {
			if (idField != null) {
				idField.setAccessible(true);
				for (T temp : getInstanceValue()) {
					result.put(idField.get(temp).toString(), temp);
				}
				idField.setAccessible(false);

			} else {// no idField --> Integer Key
				for (int i = 0; i < getInstanceValue().size(); i++) {
					result.put(String.valueOf(i), this.getInstanceValue().get(i));
				}
			}
		}
		this.setObjMap(result);
	}

	/**
	 * Synchronizes this.instanceValue with the values of Hashmap this.objMap.
	 */
	private void updateInstanceValue() {
		@SuppressWarnings("unchecked")
		List<T> newInstanceValue = new ArrayList<>();
		newInstanceValue.addAll(objMap.values());
		this.setInstanceValue(newInstanceValue);
	}

	public Map<String, T> getObjMap() {
		return objMap;
	}

	public void setObjMap(Map<String, T> objMap) {
		this.objMap = objMap;
	}

	public Field getIdField() {
		Field idField = null;
		ParameterizedType paramTypeOfList = (ParameterizedType) this.getZugehörigesFeld().getGenericType();
		Class<?> typeOfList = (Class<?>) paramTypeOfList.getActualTypeArguments()[0];
		List<Field> fieldsOfListType = Arrays.asList(typeOfList.getDeclaredFields());
		for (Field fieldOfListType : fieldsOfListType) {
			List<Annotation> annotations = Arrays.asList(fieldOfListType.getDeclaredAnnotations());
			for (Annotation ann : annotations) {
				// TODO
				if (ann.annotationType().getCanonicalName().equals(Id.class.getCanonicalName())) {
					idField = fieldOfListType;
				}
			}
		}
		return idField;
	}

	/**
	 * @param key
	 * @return Object for given Key from List<T>
	 */
	public T get(String key) {
		return this.objMap.get(key);
	}

	public void put(String key, T value) throws IllegalArgumentException, IllegalAccessException {
		this.objMap.put(key, value);
		this.updateInstanceValue();
	}

	public void remove(String key) {
		this.objMap.remove(key);
		this.updateInstanceValue();

	}

	/*
	 * Adds a new Instance to this objectList by calling the empty constructor
	 * of its declaring type
	 * 
	 * @return Returns the generated object
	 */
	public T generateNewElement() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ParameterizedType paramTypeOfList = (ParameterizedType) this.getZugehörigesFeld().getGenericType();
		Class<?> clazz = (Class<?>) paramTypeOfList.getActualTypeArguments()[0];
		Constructor<?> ctor = clazz.getConstructor();

		ctor.setAccessible(true);
		@SuppressWarnings("unchecked")
		T newObject = (T) ctor.newInstance();
		ctor.setAccessible(false);
		String key = Integer.toString(keyCount);
		if(getKeyList().size() > 0){
		 key = this.getKeyList().get(getKeyList().size()-1) + keyCount;
		}
		
		Field idFieldOfObject = getIdField();
		idFieldOfObject.setAccessible(true);
		idFieldOfObject.set(newObject, key);
		idFieldOfObject.setAccessible(false);

		
		this.put(key, newObject);
		List<T> newInstanceValue = this.getInstanceValue();
		newInstanceValue.add(newObject);
		
		keyCount++;
		return newObject;
	}

	/*
	 * Returns the key String of the given element. If objMap does not contain
	 * the given element and the element has no idField a new key is generated.
	 */
	public String getKey(T element) throws IllegalArgumentException, IllegalAccessException {
		String key = "";
		Field idField = this.getIdField();
		if (this.objMap.containsValue(element)) {
			key = this.objMap.entrySet().stream() //
					.filter(entry -> Objects.equals(element, entry.getValue())) //
					.map(Map.Entry::getKey).collect(Collectors.toSet()) //
					.iterator().next().toString();
		} else if (idField != null) {
			idField.setAccessible(true);
			key = idField.get(element).toString();
			idField.setAccessible(false);
		} else {
			key = String.valueOf(this.objMap.size());
		}
		return key;
	}

	public List<String> getKeyList() {
		return new ArrayList<String>(this.objMap.keySet());
	}

	@Override
	public String toString() {
		return "Objektliste [objMap=" + objMap + ", InstanceValue=" + getInstanceValue() + "]\n";
	}

}
