package de.dhbw.vs.jprakt.guigenerator.gruppe1;

import de.dhbw.vs.jprakt.guigenerator.annotations.Id;

public class Adresse {
	@Id
	private String type;
	private String ort;
	
	public Adresse(String type, String ort){
		this.type = type;
		this.ort = ort;
		
	}
	
	public Adresse(){
		this.type = "Daheim";
		this.ort = "Bürkstreet";
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	@Override
	public String toString() {
		return "Adresse [type=" + type + ", ort=" + ort + "]";
	}
	
	
	
}
