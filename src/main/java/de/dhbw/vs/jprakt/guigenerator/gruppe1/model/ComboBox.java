package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ComboBox extends UiElement<Enum<?>> {

	private List<Enum<?>> values;
	
	public ComboBox(Enum<?> instanceValue, Field zugehörigesFeld) {
		super(instanceValue, zugehörigesFeld);
		this.values = (List<Enum<?>>) Arrays.asList(zugehörigesFeld.getType().getEnumConstants());
	}

	public List<Enum<?>> getValues() {
		return values;
	}

	public void setValues(List<Enum<?>> values) {
		this.values = values;
	}
	
	public List<String> getEnumNames(){
		return values.stream().map(e -> e.name()).collect(Collectors.toList());
	}
	
	//sets the selected enum according to the given int
	public void setSelectedEnum(int selectedEnum) {
		String nameOfEnum = values.get(selectedEnum).name();
		this.setSelectedEnum(nameOfEnum);
	}

	//sets the selected enum according to the given name
	public void setSelectedEnum(String nameOfSelectedEnum) {
		Class<? extends Enum> enumType = (Class<? extends Enum>) this.getZugehörigesFeld().getType();
		System.out.println(nameOfSelectedEnum);
		this.setInstanceValue(Enum.valueOf(enumType, nameOfSelectedEnum));
		
	}
}
