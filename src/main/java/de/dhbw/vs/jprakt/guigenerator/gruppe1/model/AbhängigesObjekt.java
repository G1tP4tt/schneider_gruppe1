package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class AbhängigesObjekt<T> extends UiElement<T> {

	protected Datensatz daten;

	public AbhängigesObjekt(T InstanceValue, Field zugehörigesFeld)
			throws IllegalArgumentException, IllegalAccessException {
		super(InstanceValue, zugehörigesFeld);

	}

	public boolean hasDataset() {
		return this.daten != null;
	}

	public Datensatz getDatensatz() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.initData();
		return daten;
	}

	public void initData() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (this.getInstanceValue() == null) {
			Class<?> clazz = getZugehörigesFeld().getType();
			Constructor<?> ctor = clazz.getConstructor();
			ctor.setAccessible(true);

			T object = (T) ctor.newInstance();

			ctor.setAccessible(false);
			this.setInstanceValue(object);
			this.initData();

		} else {
			daten = ClassScanner.createDataset(this.getInstanceValue());
		}
	}

	@Override
	public String toString() {
		return "AbhängigesObjekt [instanceValue=" + getInstanceValue() + ",\n\t daten=" + (daten != null
				? daten.toString().replaceAll("\n", "") : daten) + "]";
	}

}
