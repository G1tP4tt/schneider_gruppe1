package de.dhbw.vs.jprakt.guigenerator.gruppe1.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import de.dhbw.vs.jprakt.guigenerator.annotations.BarChart;
import de.dhbw.vs.jprakt.guigenerator.annotations.PieChart;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Date;
import java.util.List;

public class ClassScanner {

	public static Datensatz createDataset(Object instance) throws IllegalArgumentException, IllegalAccessException {
		List<Field> fieldsOfClazz = Arrays.asList(instance.getClass().getDeclaredFields());
		List<UiElement<?>> elements = new ArrayList<>();

		for (Field field : fieldsOfClazz) {
			elements.add(generateUiElementFromField(instance, field));
		}

		Datensatz result = new Datensatz(instance);
		result.addAll(elements);
		return result;

	}

	/*
	 * For a given dataset, transformBack() sets all values for an Instance
	 * according to its dataset values and returns the object reference
	 */
	public static Object transformBack(Datensatz dataset)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException,
			NoSuchMethodException, InstantiationException, InvocationTargetException {

		for (UiElement<?> element : dataset) {
			if (element == null) {
				continue;
			}
			Field fieldOfElement = element.getZugehörigesFeld();
			if (element.getClass() == AbhängigesObjekt.class) {
				AbhängigesObjekt<?> abhängigesElement = (AbhängigesObjekt<?>) element;
				if (abhängigesElement.hasDataset()) {
					// Wenn das Abhängige Objekt einen Datensatz besitzt wird
					// die
					// Funktion Rekursiv aufgerufen.
					ClassScanner.transformBack(abhängigesElement.getDatensatz());
				}
			}
			fieldOfElement.setAccessible(true);
			fieldOfElement.set(dataset.getInstance(), element.getInstanceValue());
			fieldOfElement.setAccessible(false);

		}
		return dataset.getInstance();
	}

	private static UiElement<?> generateUiElementFromField(Object instance, Field field)
			throws IllegalArgumentException, IllegalAccessException {

		UiElement<?> generatedField = null;
		Class<?> fieldType = field.getType();

		// Für zugriff die Access flag für das feld auf true setzen
		field.setAccessible(true);

		if (fieldType == String.class) {
			// hole daten aus feld
			String value = (String) field.get(instance);
			generatedField = new StringTextfeld(value, field);
		} else if (fieldType == Double.class) {
			Double value = (Double) field.get(instance);
			generatedField = new DoubleTextfeld(value, field);
		} else if (fieldType == Integer.class) {
			Integer value = (Integer) field.get(instance);
			generatedField = new IntegerTextfeld(value, field);
		} else if (fieldType.isEnum()) {
			Enum<?> selectedValue = (Enum<?>) field.get(instance);
			generatedField = new ComboBox(selectedValue, field);
		} else if (fieldType == List.class) {
			List<?> objectList = (List<?>) field.get(instance);
			generatedField = new Objektliste<>(objectList, field);
		} else if (fieldType == Date.class) {
			Date value = (Date) field.get(instance);
			generatedField = new Datefield(value, field);
		} else if (fieldType == Map.class && hasGenericTypesForChartElement(field)) {
			if (field.getAnnotationsByType(BarChart.class).length > 0) {
				@SuppressWarnings("unchecked")
				Map<String, ? extends Number> value = (Map<String, ? extends Number>) field.get(instance);
				generatedField = new BarChartElement(value, field);
			} else if (field.getAnnotationsByType(PieChart.class).length > 0) {
				@SuppressWarnings("unchecked")
				Map<String, ? extends Number> value = (Map<String, ? extends Number>) field.get(instance);
				generatedField = new PieChartElement(value, field);
			} else {
				throw new CantCreateElementException("Can't create uiElement from Field: " + field.getName());
			}

		} else {

			Object value = field.get(instance);
			generatedField = new AbhängigesObjekt<>(value, field);

		}

		// und Access flag abschließen
		field.setAccessible(false);
		return generatedField;

	}

	/**
	 * @param field
	 * @return Returns true if the given field has the correct generic types for
	 *         a chart element.
	 */
	private static boolean hasGenericTypesForChartElement(Field field) {

		ParameterizedType type = (ParameterizedType) field.getGenericType();
		if (type.getActualTypeArguments().length == 2) {
			return String.class == type.getActualTypeArguments()[0] && isNumeric(type);
		}
		return false;
	}

	/**
	 * @param type
	 * @return Returns true if type is numeric
	 */
	private static boolean isNumeric(ParameterizedType t) {
		return isSubclassOf(t.getActualTypeArguments()[1], Number.class);
	}

	private static boolean isSubclassOf(Type t, Class<?> clazz) {
		if (t instanceof Class<?>) {

			return clazz.isAssignableFrom((Class<?>) t);
		}
		if (t instanceof ParameterizedType) {
			return isSubclassOf(((ParameterizedType) t).getRawType(), clazz);
		}
		Type[] bounds = null;
		if (t instanceof TypeVariable<?>) {
			bounds = ((TypeVariable<?>) t).getBounds();
			System.out.println("sdsds");
		}
		if (t instanceof WildcardType) {
			bounds = ((WildcardType) t).getUpperBounds();
		}

		if (bounds != null && bounds.length > 0) {
			return isSubclassOf(bounds[0], clazz);
		}
		return clazz == Object.class;
	}

}
