# README #

Gruppe 1: Analyse einer Objektinstanz mithilfe der Reflection API.
Patrice, Alexander, Vedat, Timo , Manuel, David Frey, Sven, Chris, Denesa

OFFENE AUFGABEN:

* Listen
* Enums
* Date

Für die anderen Gruppenprojekte bitte in den jeweiligen Repositories nachschauen.

* [Gruppe2](https://bitbucket.org/G1tP4tt/schneider_gruppe2)
* [Gruppe3](https://bitbucket.org/G1tP4tt/schneider_gruppe3)


### Unsere Aufgabe :-) ###

![Alt text](http://i.imgur.com/1kVcjHL.png "GuiGenerator")

	Wir analysieren eine gegebene Objektinstanz und erstellen aus den Member Variablen entsprechende UI Elemente.
	Die UI Elemente werden in einem Datensatz (ArrayList<UiElement>) an die anderen Gruppen weitergeleitet. 
	Ein Datensatz hat also ´n´ UiElemente.
	Ein UiElement kann ein String/Integer/Double Textfeld, aber auch ein weiteres Objekt repräsentieren.
	Dieses abhängige Objekt hat dann wiederrum einen eigenen Datensatz welcher die Member Variablen des abhängigen Objekts wiederspiegelt.

![Alt text](http://i.imgur.com/jGvRzm3.png "Abhängiges Objekt")

	Analog zum abhängigen Objekt erfolgt die Transformation einer Member List<>() der zu scannenden Objektinstanz.
	
![Alt text](http://i.imgur.com/afuxBZh.png)

	Eine komplette Übersicht wie wir mit den verschiedenen Feldtypen umgehen findet ihr hier:
	
![Alt text](http://i.imgur.com/TW4pSu4.png)

### Datenstruktur ###

	Kurze Übersicht einer ersten Version der Datenstruktur:
	
![Alt text](http://i.imgur.com/zfCrjiW.png)

### How do I get set up? ###

	Runterladen und in der main Methode mit einer Objektinstanz einer beliebigen Testklasse
	einen Datensatz aus dem ClassScanner erzeugen lassen.

### Contribution guidelines ###

	Pull Requests müssen reviewed werden, bevor gemerged wird!

### Who do I talk to? ###

[Patrice](https://bitbucket.org/G1tP4tt)
[Alex](https://bitbucket.org/AlNie13)